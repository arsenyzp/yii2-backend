<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 16/07/15
 * Time: 16:56
 */

namespace app\assets;


use yii\web\AssetBundle;

class BackendAsset extends AssetBundle{
    public $basePath = '/';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css'
    ];
    public $js = [

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\AdminLTE',
        'app\assets\Html5shiv',
    ];
} 