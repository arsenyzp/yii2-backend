<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 16/07/15
 * Time: 16:58
 */

namespace app\assets;


use yii\web\AssetBundle;

class Html5shiv extends AssetBundle{
    public $sourcePath = '@bower/html5shiv';
    public $js = [
        'dist/html5shiv.min.js'
    ];

    public $jsOptions = [
        'condition'=>'lt IE 9'
    ];
} 