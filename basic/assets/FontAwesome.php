<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 16/07/15
 * Time: 16:59
 */

namespace app\assets;


use yii\web\AssetBundle;

class FontAwesome extends AssetBundle{
    public $sourcePath = '@bower/font-awesome';
    public $css = [
        'css/font-awesome.min.css'
    ];

} 