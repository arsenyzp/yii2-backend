<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 16/07/15
 * Time: 16:57
 */

namespace app\assets;


use yii\web\AssetBundle;

class AdminLTE extends AssetBundle{
    public $sourcePath = '@bower/admin-lte';
    public $js = [
        'js/AdminLTE/app.js'
    ];
    public $css = [
        'css/AdminLTE.css'
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
        '\yii\jui\JuiAsset',
        '\yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontAwesome'
    ];
}