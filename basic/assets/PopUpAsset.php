<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 06/09/15
 * Time: 19:11
 */

namespace app\assets;


use yii\web\AssetBundle;

class PopUpAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/ajax-modal-popup.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
} 