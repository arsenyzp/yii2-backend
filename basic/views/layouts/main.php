<?php
/**
 * @var $this yii\web\View
 */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

?>
<?php $this->beginContent('@app/views/layouts/_base.php'); ?>
    <!-- header logo: style can be found in header.less -->
    <header class="header">
        <a href="<?= Yii::$app->homeUrl ?>" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <?= Yii::$app->name ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only"><?= Yii::t('backend', 'Toggle navigation') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-right">
                <ul class="nav navbar-nav">

                    <!-- Notifications: style can be found in dropdown.less -->
                    <li id="log-dropdown" class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-warning"></i>
                        <span class="badge bg-red">
                            <?= \app\models\SystemLog::find()->count() ?>
                        </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header"><?= Yii::t('backend', 'You have {num} log items', ['num'=>\app\models\SystemLog::find()->count()]) ?></li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <?php foreach(\app\models\SystemLog::find()->orderBy(['log_time'=>SORT_DESC])->limit(5)->all() as $logEntry): ?>
                                        <li>
                                            <a href="<?= Yii::$app->urlManager->createUrl(['/log/view', 'id'=>$logEntry->id]) ?>">
                                                <i class="fa fa-warning <?= $logEntry->level == \yii\log\Logger::LEVEL_ERROR ? 'bg-red' : 'bg-yellow' ?>"></i>
                                                <?= $logEntry->category ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                            <li class="footer">
                                <?= Html::a(Yii::t('backend', 'View all'), ['/log/index']) ?>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i>
                            <span>Admin<i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header bg-light-blue">
                                <img src="/img/anonymous.png" class="img-circle" alt="User Image" />
                                <p>
                                    Admin
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <?= Html::a(Yii::t('backend', 'Logout'), ['/site/logout'], ['class'=>'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="/img/anonymous.png" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p>Admin</p>
                        <a href="<?php echo \yii\helpers\Url::to(['/sign-in/profile']) ?>">
                            <i class="fa fa-circle text-success"></i>
                            <?= Yii::$app->formatter->asDatetime(time()) ?>
                        </a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?= app\components\widgets\Menu::widget([
                        'options'=>['class'=>'sidebar-menu'],
                        'labelTemplate' => '<a href="#">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                        'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                        'submenuTemplate'=>"\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                        'activateParents'=>true,
                        'items'=>[

                            [
                                'label'=>'Пользователи',
                                'url'=>['/clients'],
                                'icon'=>'<i class="fa fa-life-ring"></i>',
                            ],
                            [
                                'label'=>'Вопросы',
                                'url'=>['/questions'],
                                'icon'=>'<i class="fa fa-life-ring"></i>',
                            ],
                            [
                                'label'=>'Ответы пользователей',
                                'url'=>['/user_data/answers/index'],
                                'icon'=>'<i class="fa fa-life-ring"></i>',
                            ],

//                            [
//                                'label'=>'Подготовка',
//                                'icon'=>'<i class="fa fa-bar-chart-o"></i>',
//                                'options'=>['class'=>'treeview'],
//                                'items'=>[
//                                    ['label'=>'Шаблон', 'url'=>['/background'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
//                                    ['label'=>'Шрифт', 'url'=>['/fonts'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
//                                    ['label'=>'Текст', 'url'=>['/text'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
//                                    ['label'=>'Headers', 'url'=>['/elements'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
//                                ]
//                            ],



                            [
                                'label'=>Yii::t('backend', 'System'),
                                'icon'=>'<i class="fa fa-cogs"></i>',
                                'options'=>['class'=>'treeview'],
                                'items'=>[
                                    ['label'=>Yii::t('backend', 'Cache'), 'url'=>['/cache/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],

                                    [
                                        'label'=>Yii::t('backend', 'System Information'),
                                        'url'=>['/system-information/index'],
                                        'icon'=>'<i class="fa fa-angle-double-right"></i>'
                                    ],
                                    [
                                        'label'=>Yii::t('backend', 'Logs'),
                                        'url'=>['/log/index'],
                                        'icon'=>'<i class="fa fa-angle-double-right"></i>',
                                        'badge'=>\app\models\SystemLog::find()->count(),
                                        'badgeBgClass'=>'bg-red',
                                    ],
                                ]
                            ]
                        ]
                    ]) ?>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <?= $this->title ?>
                    <?php if(isset($this->params['subtitle'])): ?>
                        <small><?= $this->params['subtitle'] ?></small>
                    <?php endif; ?>
                </h1>

                <?= Breadcrumbs::widget([
                        'tag'=>'ol',
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
            </section>

            <!-- Main content -->
            <section class="content">
                <?php if(Yii::$app->session->hasFlash('alert')):?>
                    <?= \yii\bootstrap\Alert::widget([
                            'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                            'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                        ])?>
                <?php endif; ?>
                <?= $content ?>
            </section><!-- /.content -->
        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->


    <?php
    yii\bootstrap\Modal::begin([
            'headerOptions' => ['id' => 'modalHeader'],
            'header'=>'<span id="modalHeaderContent"></span>',
            'id' => 'modalClient',
            'size' => 'modal-lg',
            //keeps from closing modal with esc key or by clicking out of the modal.
            // user must click cancel or X to close
            //'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
            'closeButton' => [
                'label' => 'х',
                'class' => 'btn btn-danger btn-sm pull-right',
            ],
        ]);
    echo "<div id='modalContent'></div>";
    yii\bootstrap\Modal::end();
    ?>

<div id="loading">
    <img src="img/1.gif">
</div>

<?php $this->endContent(); ?>