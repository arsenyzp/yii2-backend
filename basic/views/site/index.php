<?php
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
  .panel-heading {
    padding: 5px 15px;
  }

  .profile-img {
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
  }
</style>
<div class="container" style="margin-top:40px">
  <div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong> Sign in to continue</strong>
        </div>
        <div class="panel-body">
          <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
              'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
              'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
          ]); ?>
          <form role="form" action="#" method="POST">
            <fieldset>
              <div class="row">
                <div class="center-block">
                  <img class="profile-img"
                       src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                  <div class="form-group">
                    <div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span>
                      <input class="form-control" placeholder="Username" name="LoginForm[username]" type="text" autofocus id="loginform-username">
                    </div>
                    <div class="col-lg-8"><p class="help-block help-block-error"></p></div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock"></i>
												</span>
                      <input class="form-control" placeholder="Password" name="LoginForm[password]" type="password" value="" id="loginform-password">
                    </div>
                  </div>
                  <div class="form-group">
                    <?= $form->field($model, 'rememberMe', [
                        'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                      ])->checkbox() ?>
                  </div>
                  <div class="form-group">
                    <input type="submit" class="btn btn-lg btn-primary btn-block" value="Sign in">
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
