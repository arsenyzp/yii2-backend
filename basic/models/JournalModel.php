<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%journal}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $date_create
 * @property integer $date_update
 * @property string $text
 */
class JournalModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%journal}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['text', 'date_create', 'date_update'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата редактирования',
            'text' => 'Текст',
        ];
    }

    public function beforeSave($insert){
        parent::beforeSave($insert);
        if($insert && $this->date_create == ""){
            $this->date_create = date("Y-m-d H:i", time(1));
        }
        if($this->date_update == ""){
            $this->date_update = date("Y-m-d H:i", time(1));
        }
        return true;
    }
}
