<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%answers}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_question
 * @property integer $date_answer
 * @property integer $date_answer_update
 * @property string $answer
 */
class AnswersModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%answers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_question'], 'integer'],
            [['answer','date_answer', 'date_answer_update'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_question' => 'Id Question',
            'date_answer' => 'Дата ответа',
            'date_answer_update' => 'Дата редактирования',
            'answer' => 'Ответ',
        ];
    }

    public function getQuestion(){
        return $this->hasOne(QuestionsModel::className(), ['id' => 'id_question']);
    }

    public function getUser(){
        return $this->hasOne(ClientsModel::className(), ['id' => 'id_user']);
    }

    public function beforeSave($insert){
        parent::beforeSave($insert);
        if($insert && $this->date_answer == ""){
            $this->date_answer = date("Y-m-d H:i", time(1));
        }
        if($this->date_answer_update == ""){
            $this->date_answer_update = date("Y-m-d H:i", time(1));
        }
        return true;
    }
}
