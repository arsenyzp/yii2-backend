<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%clients}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $token
 * @property string $date_register
 * @property string $platform
 * @property string $google_id
 */
class ClientsModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%clients}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_register'], 'safe'],
            [['email', 'password', 'token', 'platform', 'google_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Пароль',
            'token' => 'Token',
            'date_register' => 'Дата регистрации',
            'platform' => 'Платформа',
            'google_id' => 'Google',
        ];
    }

    public function beforeSave($insert){
        parent::beforeSave($insert);
        if($insert){
            $this->date_register = date("Y-m-d H:i", time(1));
        }
        return true;
    }

    public function createToken(){
        return md5($this->email.$this->password.time(1));
    }

    public static function getUserByToken($token){
        return self::find()->where(['token'=>$token])->one();
    }
}
