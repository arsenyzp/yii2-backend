<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%note}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $date_create
 * @property integer $date_update
 * @property string $text
 */
class NoteModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%note}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['text', 'date_create', 'date_update'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'text' => 'Text',
        ];
    }

    public function getUser(){
        return $this->hasOne(ClientsModel::className(), ['id_user' => 'id']);
    }

    public function beforeSave($insert){
        parent::beforeSave($insert);
        if($insert && $this->date_create == ""){
            $this->date_create = date("Y-m-d H:i", time(1));
        }
        if($this->date_update == ""){
            $this->date_update = date("Y-m-d H:i", time(1));
        }
        return true;
    }
}
