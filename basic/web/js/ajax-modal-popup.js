$(function(){

  // униеверсальный popup
 $(document).on('click', '.showModalButton', function(){
    if ($('#modalClient').data('bs.modal').isShown) {
      $('#modalClient').find('#modalContent')
        .load($(this).attr('value'));
      //dynamiclly set the header for the modal
      document.getElementById('modalHeaderContent').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
    } else {
      //if modal isn't open; open it and load content
      $('#modalClient').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));
      //dynamiclly set the header for the modal
      document.getElementById('modalHeaderContent').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
    }
    return false;
  });

  $(document).on('pjax:send', function() {
    $('#loading').show();
  });
  $(document).on('pjax:complete', function() {
    $('#loading').hide();
  });
});