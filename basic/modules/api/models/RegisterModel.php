<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 13/09/15
 * Time: 11:52
 */

namespace app\modules\api\models;


use app\models\ClientsModel;

class RegisterModel extends ClientsModel{

    public $confirm_password;

    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password', 'confirm_password'], 'required'],
            ['email', 'validateEmail'],
            ['confirm_password', 'validateConfirmPassword']
        ];
    }

    public function validateEmail($attribute, $params){
        if (!$this->hasErrors() && $this->isNewRecord) {
            $user = self::findOne(['email'=>$this->email]);
            if ($user) {
                $this->addError($attribute, 'Такой email уже есть');
            }
        }
    }

    public function validateConfirmPassword($attribute, $params){
        if (!$this->hasErrors()) {
            if ($this->password != $this->confirm_password) {
                $this->addError($attribute, 'Пароли не совпадают');
            }
        }
    }

    public function register(){
        $this->validate();
        if(count($this->errors)<1){
            $this->save();
            $this->token = $this->createToken();
            return $this->update();
        }
        return false;
    }
} 