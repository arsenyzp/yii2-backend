<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 13/09/15
 * Time: 14:47
 */

namespace app\modules\api\models;


use app\models\ClientsModel;

class TokenModel extends ClientsModel{

    public function rules()
    {
        return [
            [['google_id'], 'required'],
            [['google_id'], 'string'],
        ];
    }

    public function login(){
        $this->validate();
        if(count($this->errors)<1){
            $user = self::findOne(['google_id'=>$this->google_id]);
            if(!$user){
                $user = new self();
                $user->google_id = $this->google_id;
                $user->token = $user->createToken();
                $user->save();
            }
            $this->token = $user->token;
            \Yii::$app->session->set('token', $this->token);
            return true;
        }
        return false;
    }
} 