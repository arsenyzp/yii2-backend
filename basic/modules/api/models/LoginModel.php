<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 13/09/15
 * Time: 11:52
 */

namespace app\modules\api\models;


use app\models\ClientsModel;

class LoginModel extends ClientsModel{

    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            ['email', 'validateEmail'],
            ['password', 'validatePassword']
        ];
    }

    public function validateEmail($attribute, $params){
        if (!$this->hasErrors()) {
            $user = self::findOne(['email'=>$this->email]);
            if (!$user) {
                $this->addError($attribute, 'Не верный email');
            }
        }
    }

    public function validatePassword($attribute, $params){
        if (!$this->hasErrors()) {
            $user = self::findOne(['email'=>$this->email, 'password'=>$this->password]);
            if (!$user) {
                $this->addError($attribute, 'Не верный пароль');
            }
        }
    }

    public function login(){
        $this->validate();
        if(count($this->errors)<1){
            $user = self::findOne(['email'=>$this->email, 'password'=>$this->password]);
            $this->token = $user->token;
            \Yii::$app->session->set('token', $this->token);
            return true;
        }
        return false;
    }
} 