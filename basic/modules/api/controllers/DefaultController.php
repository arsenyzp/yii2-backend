<?php

namespace app\modules\api\controllers;

use app\models\AnswersModel;
use app\models\ClientsModel;
use app\models\JournalModel;
use app\models\NoteModel;
use app\models\QuestionsModel;
use app\models\TodoModel;
use app\modules\api\models\LoginModel;
use app\modules\api\models\RegisterModel;
use app\modules\api\models\TokenModel;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use Yii;

class DefaultController extends Controller
{
    public $enableCsrfValidation = false;

    private $user = false;

    /**
     * Проверка токена
     * @throws \yii\web\HttpException
     */
    private function checkToken(){
        $token = Yii::$app->request->post('token', false);
        // проверка наличия токена
        if(!$token || !Yii::$app->session->get('token', false)){
            Yii::$app->session->destroy();
            throw new HttpException('403');
        }
        // проверка совпадения токена с токкеном в сессии
        if($token != Yii::$app->session->get('token')){
            Yii::$app->session->destroy();
            throw new HttpException('403');
        }
        $this->user = ClientsModel::getUserByToken(Yii::$app->session->get('token'));
        if(!$this->user){
            Yii::$app->session->destroy();
            throw new HttpException('403');
        }
    }

    public function beforeAction($action){
        parent::beforeAction($action);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Регистрация
     * @return string
     */
    public function actionRegister()
    {
        $result = [
            'error' => false,
            'token'=> '',
        ];
        $model = new RegisterModel();
        if($model->load(Yii::$app->request->post(), '')){
            Yii::$app->response->setStatusCode(200);
            if($model->register()){
                $result['token'] = $model->token;
            } else {
                $result['error'] = $model->errors;
            }
        } else {
            $result['error'] = $model->errors;
        }
        return json_encode($result);
    }

    /**
     * Авторизация
     * @return string
     */
    public function actionLogin()
    {
        $result = [
            'error' => false,
            'token'=> '',
        ];
        $model = new LoginModel();
        if($model->load(Yii::$app->request->post(), '')){
            Yii::$app->response->setStatusCode(200);
            if($model->login()){
                $result['token'] = $model->token;
            } else {
                $result['error'] = $model->errors;
            }
        } else {
            $result['error'] = $model->errors;
        }
        return json_encode($result);
    }

    /**
     * Получение токена по ид
     * @return string
     */
    public function actionToken()
    {
        $result = [
            'error' => false,
            'token'=> '',
        ];
        $model = new TokenModel();
        if($model->load(Yii::$app->request->post(), '')){
            Yii::$app->response->setStatusCode(200);
            if($model->login()){
                $result['token'] = $model->token;
            } else {
                $result['error'] = $model->errors;
            }
        } else {
            $result['error'] = $model->errors;
        }
        return json_encode($result);
    }

    /**
     * Авторизация
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionLogout(){
        $token = Yii::$app->request->post('token', false);
        // проверка наличия токена
        if(!$token || !Yii::$app->session->get('token', false)){
            throw new HttpException('403');
        }
        Yii::$app->session->destroy();
        Yii::$app->response->setStatusCode(200);
        return json_encode([
                'error' => false,
                'data'=> [],
            ]);
    }

    /**
     * Получение списка вопросов
     * @return string
     */
    public function actionQuestions()
    {
        $this->checkToken();
        // получение списка вопросов
        $models = QuestionsModel::find()->all();
        $result = [
            'error' => false,
            'data'=> [],
        ];
        if(!$models){
            $result = [
                'error' => 'Нет вопросов',
                'data'=> [],
            ];
        } else {
            foreach($models as $question){
                $result['data'][] = [
                    'id' => $question->id,
                    'text' => $question->text,
                ];
            }
        }
        return json_encode($result);
    }

    /**
     * Сохранение ответа
     * @return string
     */
    public function actionAnswer(){
        $this->checkToken();
        $result = [
            'error' => false,
            'data'=> [],
        ];
        $text = Yii::$app->request->post('text', false);
        $id_question = Yii::$app->request->post('id_question', false);
        $id = Yii::$app->request->post('id', 0);
        $status = Yii::$app->request->post('status', 'init');

        // удаление
        if($status == "delete"){
            AnswersModel::deleteAll(['id'=>$id]);
            return $result;
        }
        if(!$text || !$id_question){
            $result = [
                'error' => 'Текст ответа не может быть пустым',
                'data'=> [],
            ];
        } else {
            $date_current = Yii::$app->request->post('date', false);
            if(!$date_current){
                $date_current = date("Y-m-d H:i", time(1));
            }
            $answer = false;
            if($id != 0){
                $answer = AnswersModel::find()->where(['id'=>$id, 'id_user'=>$this->user->id])->one();
            }
            if($id == 0 || !$answer){
                $answer = new AnswersModel();
            }

            $answer->date_answer = $date_current;
            $answer->id_user = $this->user->id;
            $answer->id_question = $id_question;
            $answer->answer = $text;
            $answer->date_answer_update = $date_current;
            if(!$answer->save()){
                $result['error'] = $answer->errors;
            }
            $result['data'] = ['id'=>$answer->id];
        }
        return json_encode($result);
    }

    /**
     * Сохранение заметки
     * @return array|string
     */
    public function actionNote(){
        $this->checkToken();
        $result = [
            'error' => false,
            'data'=> [],
        ];
        $text = Yii::$app->request->post('text', false);
        $id = Yii::$app->request->post('id', false);

        $status = Yii::$app->request->post('status', 'init');

        // удаление
        if($status == "delete"){
            NoteModel::deleteAll(['id'=>$id]);
            return $result;
        }

        if(!$text){
            $result = [
                'error' => 'Текст заметки не может быть пустым',
                'data'=> [],
            ];
        } else {
            $date_current = Yii::$app->request->post('date', false);
            if(!$date_current){
                $date_current = date("Y-m-d H:i", time(1));
            }

            $note = NoteModel::find()->where(['id'=>$id, 'id_user' => $this->user->id])->one();
            if(!$note){
                $note = new NoteModel();
                $note->date_create = $date_current;
                $note->id_user = $this->user->id;
            }
            $note->text = $text;
            $note->date_update = $date_current;
            if(!$note->save()){
                $result['error'] = $note->errors;
            }
            $result['data'] = ['id'=>$note->id];
        }
        return json_encode($result);
    }

    /**
     * Сохранение записи дневника
     * @return array|string
     */
    public function actionJournal(){
        $this->checkToken();
        $result = [
            'error' => false,
            'data'=> [],
        ];
        $text = Yii::$app->request->post('text', false);
        $id = Yii::$app->request->post('id', false);

        $status = Yii::$app->request->post('status', 'init');

        // удаление
        if($status == "delete"){
            JournalModel::deleteAll(['id'=>$id]);
            return $result;
        }

        if(!$text){
            $result = [
                'error' => 'Текст дневника не может быть пустым',
                'data'=> [],
            ];
        } else {
            $date_current = Yii::$app->request->post('date', false);
            if(!$date_current){
                $date_current = date("Y-m-d H:i", time(1));
            }

            $journal = JournalModel::find()->where(['id'=>$id, 'id_user' => $this->user->id])->one();
            if(!$journal){
                $journal = new JournalModel();
                $journal->date_create = $date_current;
                $journal->id_user = $this->user->id;
            }
            $journal->text = $text;
            $journal->date_update = $date_current;
            if(!$journal->save()){
                $result['error'] = $journal->errors;
            }
            $result['data'] = ['id'=>$journal->id];
        }
        return json_encode($result);
    }

    /**
     * Сохранение записи todo
     * @return array|string
     */
    public function actionTodo(){
        $this->checkToken();
        $result = [
            'error' => false,
            'data'=> [],
        ];
        $text = Yii::$app->request->post('text', false);
        $id = Yii::$app->request->post('id', false);
        $status = Yii::$app->request->post('status', 'init');

        // удаление
        if($status == "delete"){
            TodoModel::deleteAll(['id'=>$id]);
            return $result;
        }

        if(!$text){
            $result = [
                'error' => 'Текст заметки не может быть пустым',
                'data'=> [],
            ];
        } else {
            $date_current = Yii::$app->request->post('date', false);
            if(!$date_current){
                $date_current = date("Y-m-d H:i", time(1));
            }

            $todo = TodoModel::find()->where(['id'=>$id, 'id_user' => $this->user->id])->one();
            if(!$todo){
                $todo = new TodoModel();
                $todo->date_create = $date_current;
                $todo->id_user = $this->user->id;
            }
            $todo->text = $text;
            $todo->date_update = $date_current;
            $todo->status = $status;
            if(!$todo->save()){
                $result['error'] = $todo->errors;
            }
            $result['data'] = ['id'=>$todo->id];
        }
        return json_encode($result);
    }

    /**
     * Получение сохранённых данных
     * @return string
     */
    public function actionStoredData()
    {
        $this->checkToken();
        $result = [
            'error' => false,
            'data'=> [],
        ];
        $answers = AnswersModel::find()->where(['id_user' => $this->user->id])->all();
        foreach($answers as $answer){
            $result['data']['answers'][] = $answer->getAttributes();
        }
        $journals = JournalModel::find()->where(['id_user' => $this->user->id])->all();
        foreach($journals as $journal){
            $result['data']['journals'][] = $journal->getAttributes();
        }
        $notes = NoteModel::find()->where(['id_user' => $this->user->id])->all();
        foreach($notes as $note){
            $result['data']['notes'][] = $note->getAttributes();
        }
        $todos = TodoModel::find()->where(['id_user' => $this->user->id])->all();
        foreach($todos as $todo){
            $result['data']['todos'][] = $todo->getAttributes();
        }
        return json_encode($result);
    }

    /**
     * Восстановение пароля
     * @return bool|string
     * @throws \yii\web\HttpException
     */
    public function actionReset(){
        $result = [
            'error' => false,
            'data'=> [],
        ];

        $email = Yii::$app->request->post('email', false);

        if(!$email){
            throw new HttpException('403');
        }

        $user = ClientsModel::findOne([
                'email' => $email,
            ]);

        if ($user) {
//            Yii::$app->mailer->compose()
//                ->setFrom('admin@devapp.pro')
//                ->setTo('arsenyzp@gmail.com')
//                ->setSubject("Ваш пароль от приложения МЕНТОР")
//                ->setTextBody('fefere')
//                ->send();

            \mail( $email , "Ваш пароль от приложения МЕНТОР" , "Ваш пароль: ".$user->password);
        } else {
            $result['error'] = 'Пользователь не найден';
        }

        return json_encode($result);
    }
}
