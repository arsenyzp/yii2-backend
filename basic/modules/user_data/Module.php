<?php

namespace app\modules\user_data;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\user_data\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
