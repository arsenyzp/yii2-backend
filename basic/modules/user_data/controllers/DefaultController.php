<?php

namespace app\modules\user_data\controllers;

use app\components\BackendController;
use app\modules\user_data\models\AnswersModelSearch;
use app\modules\user_data\models\JournalModelSearch;
use app\modules\user_data\models\TodoModelSearch;
use yii\web\Controller;
use Yii;

class DefaultController extends BackendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionJournal($id)
    {
        $searchModel = new JournalModelSearch();
        $array = Yii::$app->request->queryParams;
        $array['JournalModelSearch']['id_user'] = $id;
        $dataProvider = $searchModel->search($array);

        return $this->renderAjax('journal', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionAnswers($id)
    {
        $searchModel = new AnswersModelSearch();
        $array = Yii::$app->request->queryParams;
        $array['AnswersModelSearch']['id_user'] = $id;
        $dataProvider = $searchModel->search($array);

        return $this->renderAjax('answers', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionTodo($id)
    {
        $searchModel = new TodoModelSearch();
        $array = Yii::$app->request->queryParams;
        $array['TodoModelSearch']['id_user'] = $id;
        $dataProvider = $searchModel->search($array);

        return $this->renderAjax('todo', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }
}
