<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TodoModel */

$this->title = 'Update Todo Model: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Todo Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="todo-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
