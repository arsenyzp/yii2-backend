<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TodoModel */

$this->title = 'Create Todo Model';
$this->params['breadcrumbs'][] = ['label' => 'Todo Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="todo-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
