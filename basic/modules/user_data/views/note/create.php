<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NoteModel */

$this->title = 'Create Note Model';
$this->params['breadcrumbs'][] = ['label' => 'Note Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="note-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
