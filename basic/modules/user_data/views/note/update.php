<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NoteModel */

$this->title = 'Update Note Model: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Note Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="note-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
