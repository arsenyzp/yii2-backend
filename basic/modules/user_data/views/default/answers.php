<?php

use yii\grid\GridView;
?>

<?php \yii\widgets\Pjax::begin([
        'options' => ['id' => 'answers'],
        'timeout' => 5000
    ]) ?>

<?php

echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
           // 'id_user',
            [
                'label' => 'Вопрос',
                'attribute' => 'id_question',
                'value' => function($model){return ($model->question) ? $model->question->text : '--';},
                'filter' => '',
            ],
           // 'id_question',
            [
                'attribute' => 'date_answer',
                'value' => 'date_answer',
                'filter' => '',
            ],
//            [
//                'attribute' => 'date_answer_update',
//                'value' => 'date_answer_update',
//                'filter' => '',
//            ],
             'answer:ntext',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

<?php \yii\widgets\Pjax::end(); ?>