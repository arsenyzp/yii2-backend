<?php
use yii\grid\GridView;
?>

<?php \yii\widgets\Pjax::begin([
        'options' => ['id' => 'journals'],
        'timeout' => 5000
    ]) ?>

<?php
echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'date_create',
                'value' => 'date_create',
                'filter' => '',
            ],
            [
                'attribute' => 'date_update',
                'value' => 'date_update',
                'filter' => '',
            ],
            'text:ntext',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

<?php \yii\widgets\Pjax::end(); ?>