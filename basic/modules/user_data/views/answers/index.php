<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user_data\models\AnswersModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ответы пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answers-model-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label' => 'Пользователь',
                'attribute' => 'id_user',
                'value' => function($model){return ($model->user) ? $model->user->email : '--';},
                'filter' => '',
            ],
            [
                'label' => 'Вопрос',
                'attribute' => 'id_question',
                'value' => function($model){return ($model->question) ? $model->question->text : '--';},
                'filter' => '',
            ],
            // 'id_question',
            [
                'attribute' => 'date_answer',
                'value' => 'date_answer',
                'filter' => '',
            ],
            [
                'attribute' => 'date_answer_update',
                'value' => 'date_answer_update',
                'filter' => '',
            ],
            'answer:ntext',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
