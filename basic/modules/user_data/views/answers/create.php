<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AnswersModel */

$this->title = 'Create Answers Model';
$this->params['breadcrumbs'][] = ['label' => 'Answers Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answers-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
