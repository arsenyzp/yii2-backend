<?php

namespace app\modules\questions;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\questions\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
