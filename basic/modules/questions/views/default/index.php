<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\questions\models\QuestionsModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-model-index">


    <p>
        <?= Html::button('Добавить', ['value' => Url::to(['create']), 'title' => 'Новый вопрос', 'class' => 'showModalButton btn btn-success']); ?>
    </p>

    <?php \yii\widgets\Pjax::begin([
            'options' => ['id' => 'questions'],
            'timeout' => 5000
        ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view', 'id' => 'questions-table'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'text:ntext',
            'order',

            [
                'label' => '',
                'format' => 'raw',
                'filter' => '',
                'value' => function($model){ return Html::button('Редактировать', ['value' => Url::to(['update', 'id'=>$model->id]), 'title' => 'Редактирование', 'class' => 'showModalButton btn btn-success']);}
            ],

            [
                'class' => \yii\grid\ActionColumn::className(),
                'template'=>'{delete}',
            ]
        ],
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>

</div>
