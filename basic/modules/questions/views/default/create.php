<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QuestionsModel */

$this->title = 'Create Questions Model';
$this->params['breadcrumbs'][] = ['label' => 'Questions Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-model-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
