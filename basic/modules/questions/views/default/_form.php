<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QuestionsModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-model-form">

    <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'create-questions-form'
            ]
        ]); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <div class="form-group">
        <?= Html::button($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'q-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php $this->registerJs('
            $("#q-save").on("click", function(){
                    $.ajax({
                    url: $("#create-questions-form").attr("action"),
                    type: "post",
                    data: $("#create-questions-form").serialize(),
                    success: function(res){

                        // отображение ошибок
                        var obj = JSON.parse(res);
                        if(obj.error){
                            for(var i in obj.error){
                                $("#questionsmodel-"+i).parent().find(".help-block-error").text(obj.error[i]);
                            }
                            return;
                        }

                        // обновление таблицы и закрытие окна

                        if($("#questions").length > 0){
                            $.pjax.reload({container: "#questions", timeout: 2000});
                        }

                        $("#modalClient [aria-hidden=true]").click()
                    }
                });
            });
        '); ?>

</div>
