<?php

namespace app\modules\questions\controllers;

use app\components\BackendController;
use Yii;
use app\models\QuestionsModel;
use app\modules\questions\models\QuestionsModelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for QuestionsModel model.
 */
class DefaultController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all QuestionsModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionsModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single QuestionsModel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QuestionsModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new QuestionsModel();

        if(Yii::$app->request->get('phone', false)){
            $model->phone = Yii::$app->request->get('phone', '');
        }

        if ($model->load(Yii::$app->request->post())) {

            if(!$model->validate() ||  !$model->save()){
                return json_encode(['error' => $model->errors]);
            }
            return json_encode(['success' => $model->id]);

        } else {
            return $this->renderAjax('create', [
                    'model' => $model,
                ]);
        }
    }

    /**
     * Updates an existing QuestionsModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if(!$model->validate() ||  !$model->save()){
                return json_encode(['error' => $model->errors]);
            }
            return json_encode(['success' => $model->id]);

        } else {
            return $this->renderAjax('update', [
                    'model' => $model,
                ]);
        }
    }

    /**
     * Deletes an existing QuestionsModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the QuestionsModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuestionsModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QuestionsModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
