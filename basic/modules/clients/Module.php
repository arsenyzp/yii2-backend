<?php

namespace app\modules\clients;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\clients\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
