<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\clients\models\ClientsModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-model-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Добавить', ['value' => Url::to(['create']), 'title' => 'Новый клиент', 'class' => 'showModalButton btn btn-success']); ?>
    </p>

    <?php \yii\widgets\Pjax::begin([
            'options' => ['id' => 'clients'],
            'timeout' => 5000
        ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view', 'id' => 'client-table'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'email:ntext',
            'date_register:ntext',
            'password:ntext',

            [
                'label' => '',
                'format' => 'raw',
                'filter' => '',
                'value' => function($model){ return Html::button('Редактировать', ['value' => Url::to(['update', 'id'=>$model->id]), 'title' => 'Редактирование', 'class' => 'showModalButton btn btn-success']);}
            ],

            [
                'label' => '',
                'format' => 'raw',
                'filter' => '',
                'value' => function($model){ return Html::button('Дневник', ['value' => Url::to(['/user_data/default/journal', 'id'=>$model->id]), 'title' => 'Дневник', 'class' => 'showModalButton btn btn-success']);}
            ],
            [
                'label' => '',
                'format' => 'raw',
                'filter' => '',
                'value' => function($model){ return Html::button('Ответы', ['value' => Url::to(['/user_data/default/answers', 'id'=>$model->id]), 'title' => 'Ответы', 'class' => 'showModalButton btn btn-success']);}
            ],
            [
                'label' => '',
                'format' => 'raw',
                'filter' => '',
                'value' => function($model){ return Html::button('TODO', ['value' => Url::to(['/user_data/default/todo', 'id'=>$model->id]), 'title' => 'Todo', 'class' => 'showModalButton btn btn-success']);}
            ],

            [
                'class' => \yii\grid\ActionColumn::className(),
                'template'=>'{delete}',
            ]
        ],
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>

</div>
