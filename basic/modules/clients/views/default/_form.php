<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-model-form">

    <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'create-client-form'
            ]
        ]); ?>

    <?= $form->field($model, 'email')->input('text') ?>

    <?= $form->field($model, 'password')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::button($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'client-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php $this->registerJs('
            $("#client-save").on("click", function(){
                    $.ajax({
                    url: $("#create-client-form").attr("action"),
                    type: "post",
                    data: $("#create-client-form").serialize(),
                    success: function(res){

                        // отображение ошибок
                        var obj = JSON.parse(res);
                        if(obj.error){
                            for(var i in obj.error){
                                $("#clientsmodel-"+i).parent().find(".help-block-error").text(obj.error[i]);
                            }
                            return;
                        }

                        // обновление таблицы и закрытие окна

                        if($("#questions").length > 0){
                            $.pjax.reload({container: "#questions", timeout: 2000});
                        }
                        $("#modalClient [aria-hidden=true]").click()
                    }
                });
            });
        '); ?>

</div>
