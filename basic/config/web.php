<?php
$params = require(__DIR__ . '/params.php');

$config = [
  'name'=>'Ментор',
  'id' => 'basic',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'timeZone' => 'Europe/Moscow',
  'components' => [
    'request' => [
      // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
      'cookieValidationKey' => 'aqUZZPp3NrYkc6HnnqIICg5QAa4HnOnw',
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'user' => [
      'identityClass' => 'app\models\User',
      'enableAutoLogin' => false,
    ],
    'i18n' => [
      'translations' => [
          'app'=>[
              'class' => 'yii\i18n\PhpMessageSource',
              'basePath'=>'@common/messages',
          ],
                    '*'=> [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath'=>'@app/messages',
                        'fileMap'=>[
                            'common'=>'common.php',
                            'backend'=>'backend.php',
                            'frontend'=>'frontend.php',
                        ]
                    ],
//          '*' => [
//              'class' => 'yii\i18n\DbMessageSource',
//              'sourceMessageTable'=>'{{%i18n_source_message}}',
//              'messageTable'=>'{{%i18n_message}}',
//              'enableCaching' => true,
//              'cachingDuration' => 3600,
//              'forceTranslation' => true,
//          ],

      ],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'mailer' => [
      'class' => 'yii\swiftmailer\Mailer',
      // send all mails to a file by default. You have to set
      // 'useFileTransport' to false and configure a transport
      // for the mailer to send real emails.
      'useFileTransport' => true,
        'messageConfig' => [
            'charset' => 'UTF-8',
            'from' => getenv('ADMIN_EMAIL')
        ]
    ],

//    'log' => [
//      'traceLevel' => YII_DEBUG ? 3 : 0,
//      'targets' => [
//        [
//          'class' => 'yii\log\FileTarget',
//          'levels' => ['error', 'warning'],
//        ],
//      ],
//    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
          'db'=>[
              'class' => 'yii\log\DbTarget',
              'levels' => ['error', 'warning'],
              'except'=>['yii\web\HttpException:*', 'yii\i18n\I18N\*'],
              'prefix'=>function () {
                      $url = !Yii::$app->request->isConsoleRequest ? Yii::$app->request->getUrl() : null;
                      return sprintf('[%s][%s]', Yii::$app->id, $url);
                  },
              'logVars'=>[],
              'logTable'=>'{{%system_log}}'
          ]
      ],
    ],
    'db' => require(__DIR__ . '/db.php'),
  ],
  'modules' => [
      'clients' => [
          'class' => 'app\modules\clients\Module',
      ],
      'questions' => [
          'class' => 'app\modules\questions\Module',
      ],
      'api' => [
          'class' => 'app\modules\api\Module',
      ],
      'user_data' => [
          'class' => 'app\modules\user_data\Module',
      ],
  ],
  'params' => $params,
];

$config['components']['log']['targets'][] = [
    'class' => 'yii\log\FileTarget',
    'levels' => ['info'],
    'categories' => ['apps'],
    'logFile' => '@app/runtime/logs/request.log',
    'maxFileSize' => 1024 * 2,
    'maxLogFiles' => 20,
];

$config['components']['log']['targets'][] = [
    'class' => 'yii\log\FileTarget',
    'levels' => ['info'],
    'categories' => ['Api'],
    'logFile' => '@app/runtime/logs/api.log',
    'maxFileSize' => 1024 * 2,
    'maxLogFiles' => 20,
];


if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'=>'yii\debug\Module',
        'allowedIPs' => ['*'],
        /*'panels'=>[
            'xhprof'=>[
                'class'=>'\trntv\debug\xhprof\panels\XhprofPanel'
            ]
        ]*/
    ];

  $config['bootstrap'][] = 'gii';
  $config['modules']['gii']['class'] = 'yii\gii\Module';
  $config['modules']['gii']['allowedIPs'] = ['*'];
}
else{
  $config['components']['cache'] = [
    'class' => 'yii\caching\FileCache',
    'cachePath' => '@app/runtime/cache'
  ];
  $config['components']['log']['targets']['email'] = [
    'class' => 'yii\log\EmailTarget',
    'except' => ['yii\web\HttpException:*'],
    'levels' => ['error', 'warning'],
    'message' => ['from' => 'admin@devapp.pro', 'to' => 'arsenyzp@gmail.com']
  ];
}
return $config;
