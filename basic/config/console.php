<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
  'id' => 'basic-console',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log', 'gii'],
  'controllerNamespace' => 'app\commands',
  'timeZone' => 'Europe/Moscow',
  'modules' => [
    'gii' => 'yii\gii\Module',
  ],
  'components' => [
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
//    'log' => [
//      'targets' => [
//        [
//          'class' => 'yii\log\FileTarget',
//          'levels' => ['error', 'warning'],
//        ],
//      ],
//    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
          'db'=>[
              'class' => 'yii\log\DbTarget',
              'levels' => ['error', 'warning'],
              'except'=>['yii\web\HttpException:*', 'yii\i18n\I18N\*'],
              'prefix'=>function () {
                      $url = !Yii::$app->request->isConsoleRequest ? Yii::$app->request->getUrl() : null;
                      return sprintf('[%s][%s]', Yii::$app->id, $url);
                  },
              'logVars'=>[],
              'logTable'=>'{{%system_log}}'
          ]
      ],
    ],
    'db' => $db,
  ],
  'params' => $params,
];

$config['components']['log']['targets']['email'] = [
  'class' => 'yii\log\EmailTarget',
  'except' => ['yii\web\HttpException:*'],
  'levels' => ['error', 'warning'],
  'message' => ['from' => 'web@app.teleastro.ru', 'to' => 'arsenyzp@gmail.com']
];
return $config;