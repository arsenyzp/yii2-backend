<?php

use yii\db\Migration;
use yii\db\Schema;

class m150912_170014_questions extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%questions}}',
            [
                'id' => Schema::TYPE_PK,
                'text' => Schema::TYPE_TEXT,
                'order' => Schema::TYPE_INTEGER,
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%questions}}');
    }
}
