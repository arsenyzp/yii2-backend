<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m150915_031138_to_do extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%answers}}',
            [
                'id' => Schema::TYPE_PK,
                'id_user' => Schema::TYPE_INTEGER,
                'id_question' => Schema::TYPE_INTEGER,
                'date_answer' => Schema::TYPE_DATETIME,
                'date_answer_update' => Schema::TYPE_DATETIME,
                'answer' => Schema::TYPE_TEXT,
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%note}}',
            [
                'id' => Schema::TYPE_PK,
                'id_user' => Schema::TYPE_INTEGER,
                'date_create' => Schema::TYPE_DATETIME,
                'date_update' => Schema::TYPE_DATETIME,
                'text' => Schema::TYPE_TEXT,
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%journal}}',
            [
                'id' => Schema::TYPE_PK,
                'id_user' => Schema::TYPE_INTEGER,
                'date_create' => Schema::TYPE_DATETIME,
                'date_update' => Schema::TYPE_DATETIME,
                'text' => Schema::TYPE_TEXT,
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%todo}}',
            [
                'id' => Schema::TYPE_PK,
                'id_user' => Schema::TYPE_INTEGER,
                'date_create' => Schema::TYPE_DATETIME,
                'date_update' => Schema::TYPE_DATETIME,
                'status' => Schema::TYPE_STRING,
                'text' => Schema::TYPE_TEXT,
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%answers}}');
        $this->dropTable('{{%note}}');
        $this->dropTable('{{%journal}}');
        $this->dropTable('{{%todo}}');
    }
}
