<?php

use yii\db\Schema;
use yii\db\Migration;

class m150906_083023_clients extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%clients}}',
            [
                'id' => Schema::TYPE_PK,
                'email' => Schema::TYPE_STRING,
                'password' => Schema::TYPE_STRING,
                'token' => Schema::TYPE_STRING,
                'date_register' => Schema::TYPE_DATETIME,
                'platform' =>Schema::TYPE_STRING,
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%clients}}');
    }
}
