<?php

use yii\db\Migration;

class m150913_074419_upd_client extends Migration
{
    public function up()
    {
        $this->addColumn('{{%clients}}', 'google_id', \yii\db\sqlite\Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%clients}}', 'google_id');
    }
}
