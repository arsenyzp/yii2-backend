<?php
/**
 * BackendController
 */

namespace app\components;
use yii\web\Controller;

class BackendController extends Controller {

  public function beforeAction($action){
    if(\Yii::$app->user->isGuest)
      return $this->goHome();
    return true;
  }
} 